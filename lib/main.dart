import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

void main() => runApp(MaterialApp(
      home: MyApp(),
    ));

class MyApp extends StatefulWidget {
  MyApp({
    Key key,
  }) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<ListItem> _dropdownItems = [
    ListItem(1, 5, "Tomatoes"),
    ListItem(2, 10, "Potatos"),
    ListItem(3, 7, "Cucumber"),
    ListItem(4, 3, "Onions")
  ];
  List<Product> items = new List<Product>();
  List<DropdownMenuItem<ListItem>> _dropdownMenuItems;
  ListItem _itemSelected;
  int totalPrice = 0;

  void initState() {
    super.initState();
    _dropdownMenuItems = buildDropDownMenuItems(_dropdownItems);
    _itemSelected = _dropdownMenuItems[1].value;
  }

  List<DropdownMenuItem<ListItem>> buildDropDownMenuItems(List listItems) {
    List<DropdownMenuItem<ListItem>> items = List();
    for (ListItem listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Text(listItem.name),
          value: listItem,
        ),
      );
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Shopping application"),
      ),
      body: Column(children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Container(
              padding: const EdgeInsets.all(5.0),
              decoration: BoxDecoration(
                  color: Colors.greenAccent, border: Border.all()),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  DropdownButtonHideUnderline(
                    child: DropdownButton(
                        value: _itemSelected,
                        items: _dropdownMenuItems,
                        onChanged: (value) {
                          setState(() {
                            _itemSelected = value;
                          });
                        }),
                  ),
                  RaisedButton(
                    child: Text(
                      "Add Item",
                      style: TextStyle(fontSize: 20),
                    ),
                    onPressed: () {
                      setState(() {
                        items.add(new Product(
                            _itemSelected.price, _itemSelected.name));
                        totalPrice += _itemSelected.price;
                      });
                    },
                    color: Colors.red,
                    textColor: Colors.yellow,
                    padding: EdgeInsets.all(8.0),
                    splashColor: Colors.grey,
                  )
                ],
              )),
        ),
        Text(
          'Total price = $totalPrice',
          style: TextStyle(fontSize: 20),
        ),
        Expanded(
          child: ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: items.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text('${items[index].name}'),
                trailing: Text('Price : ${items[index].price}'),
              );
            },
          ),
        )
      ]),
    );
  }
}

class ListItem {
  int value;
  String name;
  int price;

  ListItem(this.value, this.price, this.name);
}

class Product {
  int price;
  String name;

  Product(this.price, this.name);
}
